/* tests */
/* DO NOT MODIFY */


Notifications.notify('By default there and first (at the bottom)');

var loading = Notifications.notify('Loading...', {time:100, icon: 'imgs/arrow_refresh.png'});

setTimeout(function() {
	Notifications.notify('Arriving after 2 seconds.', {time:10});
}, 2000);

setTimeout(function() {
	try {
		if (!loading) {
			throw new Error('');
		}
		loading.remove();
	} catch (e) {
		Notifications.notify('Remove() not implemented', {time:10, type:'error'})
	}
}, 5000);

setTimeout(function() {
	Notifications.notify('<b>html</b> to stay 15 seconds', {time:15});
	Notifications.notify('Error for 10 seconds', {time:10, type:'error'});

	Notifications.notify('Example warning message', {time: 3, type:'warn'});

}, 1000);
